Feeds extra logging 7.x-1.x, xxxx-xx-xx
---------------------------------------

#2825261 by mparker17: Add a project page and README.
#2825260 by mparker17: Add a CHANGELOG file.
#2825259 by mparker17: Log when a taxonomy term is automatically created.
